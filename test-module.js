/* class calculator {
    add(a, b) {
        return a + b;
    }

    multiply(a, b) {
        return a * b;

    }

    divide(a, b) {
        return a / b;
    }
}



module.exports = calculator; */

//the previous code also we can write without the name
// of the class

module.exports = class {
    add(a, b) {
        return a + b;
    }

    multiply(a, b) {
        return a * b;

    }

    divide(a, b) {
        return a / b;
    }
};




